	<div id="sidebar">
		<ul>
			<?php 	/* Widgetized sidebar, if you have the plugin installed. */
					if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar() ) : ?>
            <li><a href="/news/">NEWS</a></li>
            <li><a href="/works/">WORKS</a></li>
            <!--<li><a href="/calendar/">FORTHCOMING EVENTS</a></li>-->
            
            <li class="spaceabove"><a href="/biography/">BIOGRAPHY</li>
            <li><a href="/discography/">DISCOGRAPHY</a></li>
            <li><a href="/cv/">CV</a></li>
            
            <li class="spaceabove"><a href="http://annetteworks.com/">ANNETTEWORKS</a></li>
			<?php endif; ?>
		</ul>
	</div>

