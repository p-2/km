<?php
/*
Template Name: Works
*/
if (isset($_SERVER["HTTP_X_REQUESTED_WITH"]) && $_SERVER["HTTP_X_REQUESTED_WITH"] == "XMLHttpRequest") {
    //ajax call
    if (have_posts()) : while (have_posts()) : the_post();
          $custom_fields = get_post_custom();
          if (isset($custom_fields['redirect'])) {
          	  print($custom_fields['redirect'][0]);
          } else {
          	  print('<div class="ajaxcontent">');
              the_content('');
              print('</div>');
			  wp_footer();
          }
    endwhile;
    endif;
    exit;
}
?>
<?php get_header(); ?>

	<div id="content">
	<!--  this is the template for works!! -->

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
    <?php the_title('<h2 class="page-title">', '</h2>'); ?>
    <?php
    $children = wp_list_pages('title_li=&child_of='.$post->ID.'&sort_column=menu_order&echo=0');
    if ($children) { ?>
      <ul id="works-list">
      <?php echo str_replace("<a ", '<a class="worktitle" ', $children); ?>
      </ul>
    <?php } ?>
    <?php if ($post->post_parent) { ?>
      <p class="subpage-link"><a href="<?php echo get_permalink($post->post_parent) ?>">go back</a></p>
    
    <?php } else { ?>
      <script type="text/javascript" src="/js/jquery-1.2.6.min.js"></script>
      <script type="text/javascript">
      $(function(){
          var pageHash = {};
          $(".worktitle").click(function() {
              if (this.blur) this.blur();
              var originator = $(this).parent();
              var pageKey = $(this).attr("href");
              if ($('.ajaxcontent', originator).length) {
                  //content already loaded - show/hide
                  $('.ajaxcontent', originator).slideToggle('slow');
              } else if (pageHash[pageKey]) {
                  // content stored in pageHash object
                  originator.append(pageHash[pageKey]).hide().slideDown('slow');
              } else {
                  // get page via ajax
                  $.get($(this).attr("href"), '', function(data, textStatus) {
                      if (data.indexOf("http") === 0) {
                          window.location.href = data;
                          return;
                      }
                      pageHash[pageKey] = data;
                      $("html,body").animate({ scrollTop: originator.offset().top }, "slow", "swing");
                      originator.append(data).hide().slideDown('slow');
                  });
              }
              //alert($(this).attr("href"));
              return false;
          });
      });
      </script>
    <?php } ?>
			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>
		</div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>