<?php get_header(); ?>

	<div id="content">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
        <?php
        the_title('<h2 class="page-title">', '</h2>');
        ?>
    <?php
    $children = wp_list_pages('title_li=&child_of='.$post->ID.'&sort_column=menu_order&echo=0');
    if ($children) { ?>
      <ul id="subpage-list">
      <?php echo $children; ?>
      </ul>
    <?php } ?>
    
    <?php if ($post->post_parent) { ?>
      <p class="subpage-link"><a href="<?php echo get_permalink($post->post_parent) ?>">go back</a></p>
    
    <?php } ?>
			<div class="entry">
				<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>

			</div>
		</div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>