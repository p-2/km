<?php
/*
Template Name: Calendar
*/
?>
<?php get_header(); ?>

	<div id="content">

        <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">

        <?php the_title('<h1>', '</h1>');
        ec3_get_calendar("ec3default",1);
        ?>
			<div class="entry">
				<?php the_content(''); ?>
			</div>
		</div>
		<?php endwhile; endif; ?>
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>
	</div>
      <script type="text/javascript" src="/js/jquery-1.2.6.min.js"></script>
      <script type="text/javascript">
      var pageHash = {};
      var closebutton;
      $(function(){
          var currentPage = '';
          closebutton = $('<div><a id="closebutton" href="#" onclick="hidePopup();">&nbsp;</a></div>');
          $('<div/>').attr("id", "eventpopup").appendTo('body');
          activateLinks();
          $("#ec3_prev, #ec3_next").click(function() {$('#eventpopup').hide();});
      });
      var hidePopup = function() {
          $('#eventpopup').hide();
      }
      var activateLinks = function() {
          $("a.eventday").click(function(e){
              $('#eventpopup').hide().empty().css({left:(e.pageX + 10),top:(e.pageY + 10)});
              currentPage = $(this).attr("href");
              if (pageHash[currentPage]) {
                  $('#eventpopup').append(closebutton);
                  $('#eventpopup').append(pageHash[currentPage]).fadeIn('fast');
              } else {
                  $('#eventpopup').load(currentPage, '', function(){pageHash[currentPage]=$('#eventpopup').html();$("#eventpopup").prepend(closebutton);$(this).fadeIn('fast');$('#eventpopup a[@rel="bookmark"]').css({cursor:'default'}).attr({"title":''}).click(function(){return false;});});
              }
              return false;
          }); 
      }
      </script>
	

<?php get_sidebar(); ?>

<?php get_footer(); ?>